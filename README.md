# Vietnamese ABSA

Aspect Based Sentiment Analysis (ABSA) for Vietnamese

# Requirements
Run
```
conda env create -f environment.yml
```

# Training
## Dataset
This repository of mine just supports [VLSP2018-SA dataset](https://vlsp.org.vn/vlsp2018/eval/sa).
If you want to use your own dataset, at first, you should modify json files in [json_config](Data/json_config).

## Preprocess data
```
python preprocess_data.py \
-d Data/VLSP2018-SA/restaurant_train_set.csv \
-o Data/VLSP2018-SA_new/ \
-t restaurant \
-n train \
-u 
```
Run this command for train, dev, test set respectively.

**Note:** Only use upsampling for train set. Remove -u to turn off upsampling.

## Prepare BERT pytorch model
Download [BERT-multilingual](https://github.com/google-research/bert) and then convert a tensorflow checkpoint to a pytorch model
```
python convert_tf_checkpoint_to_pytorch.py \
--tf_checkpoint_path multi_cased_L-12_H-768_A-12/bert_model.ckpt \
--bert_config_file multi_cased_L-12_H-768_A-12/bert_config.json \
--pytorch_dump_path multi_cased_L-12_H-768_A-12/pytorch_model.bin
```

## Train model
Run
```
python train.py \
--data_dir Data/VLSP2018-SA_new/ \
--type restaurant \
--output_dir checkpoint/ \
--bert_dir multi_cased_L-12_H-768_A-12/
```

## Evaluate model
Run
```
python evaluate.py \
--model_dir checkpoint/ \
--data_dir Data/VLSP2018-SA_new/ \
--type restaurant \
--bert_dir multi_cased_L-12_H-768_A-12/
```

## Inference
Run
```
python inference.py \
--model_dir checkpoint/ \
--sent "đồ ăn ngon tuyệt vời" \
--type "restaurant" \
--config_dir Data/json_config \
--bert_dir multi_cased_L-12_H-768_A-12/
```