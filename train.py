import logging
import os
import random
import numpy as np
import argparse
import collections
from tqdm import tqdm, trange
import torch
from torch.nn import CrossEntropyLoss, BCELoss
from torch.utils.data import DataLoader, TensorDataset
from torch.utils.data.sampler import WeightedRandomSampler
import tokenization
from modeling import BertConfig, BertForSequenceClassification_HEAT
from optimization import BERTAdam
from processor import VN_ABSA_pair_Processor

logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(name)s -   %(message)s', 
                    datefmt = '%m/%d/%Y %H:%M:%S',
                    level = logging.INFO)
logger = logging.getLogger(__name__)

class InputFeatures_HEAT():
    def __init__(self, input_ids, input_mask, segment_ids, sent_ids, asp_ids, asp_label=None, label_id=None):
        self.input_ids = input_ids
        self.input_mask = input_mask
        self.segment_ids = segment_ids
        self.sent_ids = sent_ids
        self.asp_ids = asp_ids
        self.asp_label = asp_label
        self.label_id = label_id

def _truncate_seq_pair(tokens_a, tokens_b, max_length):
    """Truncates a sequence pair in place to the maximum length."""

    # This is a simple heuristic which will always truncate the longer sequence
    # one token at a time. This makes more sense than truncating an equal percent
    # of tokens from each, since if one sequence is very short then each token
    # that's truncated likely contains more information than a longer sequence.
    while True:
        total_length = len(tokens_a) + len(tokens_b)
        if total_length <= max_length:
            break
        elif len(tokens_a) > len(tokens_b):
            tokens_a.pop()
        else:
            tokens_b.pop()

def convert_examples_to_features_HEAT(examples, label_map, max_seq_length, tokenizer):
    """
        Load a data file into a list of InputBatchs.
    """

    # label_map = {}
    # for i, label in enumerate(label_list):
    #     label_map[label] = i

    features = []
    for index, example in enumerate(tqdm(examples)):
        tokens_a = tokenizer.tokenize(example.text_a)

        tokens_b = None
        if example.text_b:
            # Check whether it is bert pair case or bert single case
            tokens_b = tokenizer.tokenize(example.text_b)

        if tokens_b:
            # BERT pair case
            # Modifies `tokens_a` and `tokens_b` in place so that the total
            # length is less than the specified length.
            # Account for [CLS], [SEP], [SEP] with "- 3"
            _truncate_seq_pair(tokens_a, tokens_b, max_seq_length - 3)
        else:
            # BERT single case
            # Account for [CLS] and [SEP] with "- 2"
            if len(tokens_a) > max_seq_length - 2:
                tokens_a = tokens_a[0:(max_seq_length - 2)]

        # The convention in BERT is:
        # (a) For sequence pairs:
        #  tokens:   [CLS] is this jack ##son ##ville ? [SEP] no it is not . [SEP]
        #  type_ids: 0   0  0    0    0     0       0 0    1  1  1  1   1 1
        # (b) For single sequences:
        #  tokens:   [CLS] the dog is hairy . [SEP]
        #  type_ids: 0   0   0   0  0     0 0
        #
        # Where "type_ids" are used to indicate whether this is the first
        # sequence or the second sequence. The embedding vectors for `type=0` and
        # `type=1` were learned during pre-training and are added to the wordpiece
        # embedding vector (and position vector). This is not *strictly* necessary
        # since the [SEP] token unambigiously separates the sequences, but it makes
        # it easier for the model to learn the concept of sequences.
        #
        # For classification tasks, the first vector (corresponding to [CLS]) is
        # used as as the "sentence vector". Note that this only makes sense because
        # the entire model is fine-tuned.
        tokens = []
        segment_ids = []
        sent_ids = []
        asp_ids = []
        tokens.append("[CLS]")
        segment_ids.append(0)
        sent_ids.append(0)
        asp_ids.append(0)
        for token in tokens_a:
            tokens.append(token)
            segment_ids.append(0)
            sent_ids.append(1)
            asp_ids.append(0)
        tokens.append("[SEP]")
        segment_ids.append(0)
        sent_ids.append(0)
        asp_ids.append(0)

        if tokens_b:
            # BERT pair case
            for token in tokens_b:
                tokens.append(token)
                segment_ids.append(1)
                sent_ids.append(0)
                asp_ids.append(1)
            tokens.append("[SEP]")
            segment_ids.append(1)
            sent_ids.append(0)
            asp_ids.append(0)

        input_ids = tokenizer.convert_tokens_to_ids(tokens)

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        input_mask = [1] * len(input_ids)

        # Zero-pad up to the sequence length.
        while len(input_ids) < max_seq_length:
            input_ids.append(0)
            input_mask.append(0)
            segment_ids.append(0)
            sent_ids.append(0)
            asp_ids.append(0)

        assert len(input_ids) == max_seq_length
        assert len(input_mask) == max_seq_length
        assert len(segment_ids) == max_seq_length
        assert len(sent_ids) == max_seq_length
        assert len(asp_ids) == max_seq_length

        label_id = label_map[example.label]
        if label_id != 3:
            asp_label = 1
        else:
            asp_label = 0

        features.append(
                InputFeatures_HEAT(
                        input_ids=input_ids,
                        input_mask=input_mask,
                        segment_ids=segment_ids,
                        sent_ids=sent_ids,
                        asp_ids=asp_ids,
                        asp_label=asp_label,
                        label_id=label_id))

    return features

def make_weights_for_balanced_class(all_label_ids, nclasses):
    count = [0] * nclasses
    for item in all_label_ids:
        count[item] += 1
    weights_per_class = [0.] * nclasses
    N = float(sum(count))
    for i in range(nclasses):
        weights_per_class[i] = N/float(count[i])
    weight = [0] * len(all_label_ids)
    for idx, val in enumerate(all_label_ids):
        weight[idx] = weights_per_class[val]

    return weight

def pretrain(args):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        
    # PRETRAIN PHASE
    # Prepare model
    processor = VN_ABSA_pair_Processor()
    label_map = processor.get_labels(args.config_dir)
    bert_config = BertConfig.from_json_file(os.path.join(args.bert_dir, "bert_config.json"))
    model = BertForSequenceClassification_HEAT(bert_config, len(label_map))
    model.bert.load_state_dict(torch.load(os.path.join(args.bert_dir, "pytorch_model.bin"), map_location='cpu'))
    model.to(device)

    tokenizer = tokenization.FullTokenizer(
        vocab_file=os.path.join(args.bert_dir, "vocab.txt"),
        do_lower_case=True
    )

    # training set
    train_examples = processor.get_train_examples(args.data_dir, args.type)
    num_train_steps = int(len(train_examples) / args.train_batch_size * args.pretrain_epochs)

    train_features = convert_examples_to_features_HEAT(
        train_examples, label_map, args.pretrain_len, tokenizer
    )

    logger.info("***** Running pretrain phase *****")
    logger.info("  Num examples = %d", len(train_examples))
    logger.info("  Batch size = %d", args.train_batch_size)
    logger.info("  Num steps = %d", num_train_steps)

    all_input_ids = torch.tensor([f.input_ids for f in train_features], dtype=torch.long)
    all_input_mask = torch.tensor([f.input_mask for f in train_features], dtype=torch.long)
    all_segment_ids = torch.tensor([f.segment_ids for f in train_features], dtype=torch.long)
    all_sent_ids = torch.tensor([f.sent_ids for f in train_features], dtype=torch.float)
    all_asp_ids = torch.tensor([f.asp_ids for f in train_features], dtype=torch.float)
    all_asp_label = torch.tensor([f.asp_label for f in train_features], dtype=torch.float)
    all_label_ids = torch.tensor([f.label_id for f in train_features], dtype=torch.long)
    weights_sampler = make_weights_for_balanced_class(all_label_ids, len(label_map))
    
    train_data = TensorDataset(all_input_ids, all_input_mask, all_segment_ids, 
                               all_sent_ids, all_asp_ids,
                               all_asp_label, all_label_ids)
    train_sampler = WeightedRandomSampler(weights_sampler, len(weights_sampler))
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=args.train_batch_size)

    # Optimizer
    no_decay = ['bias', 'gamma', 'beta']
    optimizer_parameters = [
         {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)], 'weight_decay_rate': 0.01},
         {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay_rate': 0.0}
         ]
		
    optimizer = BERTAdam(optimizer_parameters,
                         lr=args.lr,
                         warmup=args.warmup_proportion,
                         t_total=num_train_steps)

    # train
    loss_pol_fct = CrossEntropyLoss()
    loss_asp_fct = BCELoss()
    for name, p in model.named_parameters():
        if "bert" in name:
            p.requires_grad = False
    for _ in trange(int(args.pretrain_epochs), desc="Epoch"):
        model.train()
        for step, batch in enumerate(tqdm(train_dataloader, desc="Iteration")):
            batch = tuple(t.to(device) for t in batch)
            input_ids, input_mask, segment_ids, sent_ids, asp_ids, asp_label, label_ids = batch
            logits_asp, logits_senti, _, _ = model(input_ids, segment_ids, input_mask, sent_ids, asp_ids)
            loss_asp = loss_asp_fct(logits_asp, asp_label)
            loss_senti = loss_pol_fct(logits_senti, label_ids)
            reg_loss = None
            for param in model.parameters():
                if reg_loss is None:
                    reg_loss = 0.5 * torch.sum(param**2)
                else:
                    reg_loss = reg_loss + 0.5 * param.norm(2)**2
            # Calculate loss
            total_loss = loss_asp + loss_senti + args.l2_weight*reg_loss
            # Back-propagation
            total_loss.backward()
            optimizer.step()
            # Free gradient for next batch
            model.zero_grad()

    print("DONE PRETRAINING!!!!!!")

    return model

def finetune(args, model):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # FINETUNE PHASE
    processor = VN_ABSA_pair_Processor()
    label_map = processor.get_labels(args.config_dir)
    tokenizer = tokenization.FullTokenizer(
        vocab_file=os.path.join(args.bert_dir, "vocab.txt"),
        do_lower_case=True
    )

    # Train set
    train_examples = processor.get_train_examples(args.data_dir, args.type)
    train_features = convert_examples_to_features_HEAT(
        train_examples, label_map, args.finetune_len, tokenizer
    )
    num_train_steps = int(len(train_examples) / args.train_batch_size * args.finetune_epochs)

    logger.info("***** Running Finetune Phase *****")
    logger.info("  Num examples = %d", len(train_examples))
    logger.info("  Batch size = %d", args.train_batch_size)
    logger.info("  Num steps = %d", num_train_steps)

    all_input_ids = torch.tensor([f.input_ids for f in train_features], dtype=torch.long)
    all_input_mask = torch.tensor([f.input_mask for f in train_features], dtype=torch.long)
    all_segment_ids = torch.tensor([f.segment_ids for f in train_features], dtype=torch.long)
    all_sent_ids = torch.tensor([f.sent_ids for f in train_features], dtype=torch.float)
    all_asp_ids = torch.tensor([f.asp_ids for f in train_features], dtype=torch.float)
    all_asp_label = torch.tensor([f.asp_label for f in train_features], dtype=torch.float)
    all_label_ids = torch.tensor([f.label_id for f in train_features], dtype=torch.long)
    weights_sampler = make_weights_for_balanced_class(all_label_ids, len(label_map))
    
    train_data = TensorDataset(all_input_ids, all_input_mask, all_segment_ids, 
                               all_sent_ids, all_asp_ids,
                               all_asp_label, all_label_ids)
    train_sampler = WeightedRandomSampler(weights_sampler, len(weights_sampler))
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=args.train_batch_size)

    # Test set
    test_examples = processor.get_dev_examples(args.data_dir, args.type)
    test_features = convert_examples_to_features_HEAT(
        test_examples, label_map, args.finetune_len, tokenizer)

    all_input_ids = torch.tensor([f.input_ids for f in test_features], dtype=torch.long)
    all_input_mask = torch.tensor([f.input_mask for f in test_features], dtype=torch.long)
    all_segment_ids = torch.tensor([f.segment_ids for f in test_features], dtype=torch.long)
    all_sent_ids = torch.tensor([f.sent_ids for f in test_features], dtype=torch.float)
    all_asp_ids = torch.tensor([f.asp_ids for f in test_features], dtype=torch.float)
    all_asp_label = torch.tensor([f.asp_label for f in test_features], dtype=torch.float)
    all_label_ids = torch.tensor([f.label_id for f in test_features], dtype=torch.long)

    test_data = TensorDataset(all_input_ids, all_input_mask, all_segment_ids,
                                all_sent_ids, all_asp_ids,
                                all_asp_label, all_label_ids)
    test_dataloader = DataLoader(test_data, batch_size=args.test_batch_size, shuffle=False)
    
    # New Optimizer
    no_decay = ['bias', 'gamma', 'beta']
    optimizer_parameters = [
         {'params': [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)], 'weight_decay_rate': 0.01},
         {'params': [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)], 'weight_decay_rate': 0.0}
         ]

    optimizer = BERTAdam(optimizer_parameters,
                         lr=args.lr,
                         warmup=args.warmup_proportion,
                         t_total=num_train_steps)

    # Log file
    output_log_file = os.path.join(args.output_dir, "logs.txt")
    print("output_log_file = ", output_log_file)
    with open(output_log_file, "w") as writer:
        writer.write("epoch\tglobal_step\tloss\ttest_loss\ttest_accuracy\n")

    global_step = 0
    epoch = 0
    list_of_test_acc = []
    early_stopping_steps = 0
    best_acc = None
    loss_pol_fct = CrossEntropyLoss()
    loss_asp_fct = BCELoss()
    for name, p in model.named_parameters():
        p.requires_grad = True
    for _ in trange(int(args.finetune_epochs), desc="Epoch"):
        if early_stopping_steps == args.patient_step:
            print("Early Stopping...")
            break
        epoch += 1
        model.train()
        tr_loss = 0
        nb_tr_examples, nb_tr_steps = 0, 0
        for step, batch in enumerate(tqdm(train_dataloader, desc="Iteration")):
            batch = tuple(t.to(device) for t in batch)
            input_ids, input_mask, segment_ids, sent_ids, asp_ids, asp_label, label_ids = batch
            logits_asp, logits_senti, _, _ = model(input_ids, segment_ids, input_mask, sent_ids, asp_ids)
            # logits = F.softmax(logits, dim=-1)
            loss_asp = loss_asp_fct(logits_asp, asp_label)
            loss_senti = loss_pol_fct(logits_senti, label_ids)
            reg_loss = None
            for param in model.parameters():
                if reg_loss is None:
                    reg_loss = 0.5 * torch.sum(param**2)
                else:
                    reg_loss = reg_loss + 0.5 * param.norm(2)**2
            # Loss
            total_loss = loss_asp + loss_senti + args.l2_weight*reg_loss
            # Back-propagation
            total_loss.backward()            
            tr_loss += total_loss.item()
            nb_tr_examples += input_ids.size(0)
            nb_tr_steps += 1
            # Optimize parameters
            optimizer.step()
            model.zero_grad()
            global_step += 1

        print("DONE FINETUNING TRAIN !!!!!!!!")
        
        # eval_test
        model.eval()
        test_loss, test_accuracy = 0, 0
        nb_test_steps, nb_test_examples = 0, 0
        for step, batch in enumerate(tqdm(test_dataloader)):
            batch = tuple(t.to(device) for t in batch)
            input_ids, input_mask, segment_ids, sent_ids, asp_ids, asp_label, label_ids = batch
            with torch.no_grad():
                logits_asp, logits_senti, _, _ = model(input_ids, segment_ids, input_mask, sent_ids, asp_ids)
            tmp_test_asp_loss = loss_asp_fct(logits_asp, asp_label)
            tmp_test_senti_loss = loss_pol_fct(logits_senti, label_ids)
            tmp_test_loss = tmp_test_asp_loss + tmp_test_senti_loss

            logits_senti = logits_senti.detach().cpu().numpy()
            label_ids = label_ids.to('cpu').numpy()
            outputs = np.argmax(logits_senti, axis=1)
            tmp_test_accuracy=np.sum(outputs == label_ids)

            test_loss += tmp_test_loss.mean().item()
            test_accuracy += tmp_test_accuracy

            nb_test_examples += input_ids.size(0)
            nb_test_steps += 1

        test_loss = test_loss / nb_test_steps
        test_accuracy = test_accuracy / nb_test_examples

        list_of_test_acc.append(test_accuracy)

        result = collections.OrderedDict()
        result = {'epoch': epoch,
                'global_step': global_step,
                'loss': tr_loss/nb_tr_steps,
                'test_loss': test_loss,
                'test_accuracy': test_accuracy}

        logger.info("***** Eval results *****")
        with open(output_log_file, "a+") as writer:
            for key in result.keys():
                logger.info("  %s = %s\n", key, str(result[key]))
                writer.write("%s\t" % (str(result[key])))
            writer.write("\n")

        # early stopping set up
        if epoch == 1:
            torch.save(model, os.path.join(args.output_dir, f"{args.type}_model"))
            best_acc = list_of_test_acc[-1]
        else:
            if list_of_test_acc[-1] <= best_acc:
                print(f"val_accuracy doesn't improve from {best_acc}")
                early_stopping_steps += 1
            else:
                print(f"val_accuracy improve from {best_acc} to {list_of_test_acc[-1]}")
                torch.save(model, os.path.join(args.output_dir, f"{args.type}_model"))
                early_stopping_steps = 0
                best_acc = list_of_test_acc[-1]
        
    print("DONE TRAINING !!!")
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='',
                        required=True, help="path to dataset directory.")
    parser.add_argument('--type', type=str, default='restaurant',
                        required=True, help="Domain of the dataset.")
    parser.add_argument('--output_dir', type=str, default='',
                        required=True, help="output directory to save checkpoint.\nCheckpoint is named after data type.")
    parser.add_argument('--bert_dir', type=str, default='',
                        required=True, help="path to BERT model directory.")
    parser.add_argument('--pretrain_epochs', type=int, default=20,
                        help="pretrain phase epochs.")
    parser.add_argument('--finetune_epochs', type=int, default=100,
                        help="finetune phase epochs.")
    parser.add_argument('--config_dir', type=str, default='Data/json_config',
                        help="directory contains config json files.")
    parser.add_argument('--train_batch_size', type=int, default=32,
                        help="train batch size.")
    parser.add_argument('--test_batch_size', type=int, default=16,
                        help="test batch size.")
    parser.add_argument('--seed', type=int, default=42,
                        help="seed.")
    parser.add_argument('--pretrain_len', type=int, default=200,
                        help="max sequence length at pretrain phase.")
    parser.add_argument('--finetune_len', type=int, default=150,
                        help="max sequence length at finetune phase.")
    parser.add_argument('--l2_weight', type=float, default=0.001,
                        help="regularization weight.")
    parser.add_argument('--lr', type=float, default=2e-5,
                        help="learning rate.")
    parser.add_argument('--warmup_proportion', type=float, default=0.1,
                        help="warmup proportion.")
    parser.add_argument('--patient_step', type=int, default=10,
                        help="early stopping setup.")

    args = parser.parse_args()
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)

    model = pretrain(args)
    finetune(args, model)