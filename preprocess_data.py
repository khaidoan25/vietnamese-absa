from utils import normalize_sent
import pandas as pd
import json
import os
import argparse

def create_balance_weight(count_per_class):
    pos_count = count_per_class["positive"]
    neg_count = count_per_class["negative"]
    neu_count = count_per_class["neutral"]
    highest_count = max((pos_count), (neg_count), (neu_count))
    balance_weight = {
        "positive": int(highest_count/pos_count) - 1,
        "negative": int(highest_count/neg_count) - 1,
        "neutral": int(highest_count/neu_count) - 1
    }

    return balance_weight


def preprocess(args):
    dataset = pd.read_csv(args.data_path)
    sentiments = ["positive", "negative", "neutral"]
    doc = dataset["Doc"]
    aspects_en2vi = json.load(open(f"Data/json_config/convert_{args.type}_label.json", "r", encoding="utf-8"))
    aspects = json.load(open(f"Data/json_config/{args.type}_aspects_en.json", "r"))

    # Convert from multi-label to multi-class classification
    new_aspects = []
    new_sentiments = []
    for label in dataset["Labels"]:
        for aspect in aspects:
            # Convert aspect from en to vi
            new_aspects.append(aspects_en2vi[aspect])
            is_none = True
            for sentiment in sentiments:
                if f"{aspect}, {sentiment}" in label:
                    new_sentiments.append(sentiment)
                    is_none = False
            if is_none:
                new_sentiments.append("none")

    reps = len(aspects)
    new_doc = normalize_sent(doc)
    new_doc = [x for x in new_doc for _ in range(reps)]

    new_dataset = pd.DataFrame()
    new_dataset["Doc"] = new_doc
    new_dataset["Aspect"] = new_aspects
    new_dataset["Sentiment"] = new_sentiments

    # Upsample imbalanced class
    if args.upsampling:
        count_per_class = new_dataset.groupby("Sentiment").count()["Doc"]
        balance_weight = create_balance_weight(count_per_class)
        for k, v in balance_weight.items():
            if v != 0:
                new_dataset = new_dataset.append([new_dataset[new_dataset["Sentiment"] == k]]*v)
        
        new_dataset = new_dataset.sample(frac=1)
    new_dataset.to_csv(os.path.join(args.output_dir, f"{args.type}_{args.set_name}_set_new.csv"), index=False, sep="\t")

    print("Done preprocess!!!")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--data_path', type=str, default='',
                        required=True, help="path to dataset.")
    parser.add_argument('-o', '--output_dir', type=str, default='',
                        required=True, help='path to save new dataset.\nNew dataset is named after type of dataset.')
    parser.add_argument('-t', '--type', type=str, default='restaurant',
                        help='domain of the dataset, default is restaurant.')
    parser.add_argument('-n', '--set_name', type=str, choices=['train', 'test', 'dev'],
                        help='name of dataset (train | test | dev).')
    parser.add_argument('-u', '--upsampling', default=False,
                        action="store_true", help='whether use upsampling method or not, default is True.\nJust use this option for train set')
	
    args = parser.parse_args()
    preprocess(args)