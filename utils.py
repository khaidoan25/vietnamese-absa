# -*- coding: utf-8 -*-
import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.abspath(__file__), "..")))
import pandas as pd
import json
import re
from tqdm import tqdm
import numpy as np
import unicodedata

def normalize_sent(sents):
    log_file = open("log_normalize.txt", "w")
    new_sents = []
    for sent in sents:
        sent = unicodedata.normalize("NFC", sent)
        sent = sent.lower()
        # Replace hashtag
        sent = re.sub(r"\#\w+", " ", sent)
        # Replace urls
        sent = re.sub(r"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$", "trang_web", sent)
        # Replace money
        sent = re.sub(r"(\d+k)|(\d+\.\d+đ)", "giá tiền", sent)
        sent = sent.replace('_', "")
        # Remove special character
        try:
            new_sent = " ".join(re.findall(r"\b[a-zoàáâãèéêếìíiuyòóôõùúăđĩũơưăạảấầẩẫậắằẳẵặẹẻẽềểễệỉịọỏốồổỗộớờởỡợụủứừửữựỳỵỷỹý0-9]+\b|[,.!]", sent))
            if len(sent) >= 2:
                sent = new_sent
            else:
                continue
        except:
            log_file.write(sent + '\n')
            continue
        sent = re.sub(r"toẹt", "tuyệt", sent)
        sent = re.sub(r"z", "d", sent)
        sent = re.sub(r"zời|dời", "vời", sent)
        sent = re.sub(r"\b(kg|k|ko|hk|khg|khong|hem|hổng|hong|hok)\b", "không", sent)
        sent = re.sub(r"thoy|hoy|thoi", "thôi", sent)
        sent = re.sub(r"nchung", "nói chung", sent)
        sent = re.sub(r"zui|dui", "vui", sent)
        sent = re.sub(r"nhìu", "nhiều", sent)
        sent = re.sub(r"cx|cg", "cũng", sent)
        sent = re.sub(r"dc|đc", "được", sent)
        sent = re.sub(r"\bj\b", "gì", sent)
        sent = re.sub(r"qtqđ", "quá trời quá đất", sent)
        sent = re.sub(r"bthuong|\bbth\b", "bình thường", sent)
        sent = re.sub(r"bik|\bbt\b", "biết", sent)
        sent = re.sub(r"\bwc\b", "nhà vệ sinh", sent)
        sent = re.sub(r"\bm\b", "mình", sent)
        sent = re.sub(r"\bngta\b", "người ta", sent)
        sent = re.sub(r"\bvl\b", "kinh khủng", sent)
        sent = re.sub(r"qá", "quá", sent)
        sent = re.sub(r"thik", "thích", sent)
        sent = re.sub(r"\bkbh\b", "không bao giờ", sent)
        sent = re.sub(r"\btrl\b", "trả lời", sent)
        sent = re.sub(r"\b(bh|bao h|bao g)\b", "bao giờ", sent)
        
        new_sents.append(sent)
        
        # sent = putSpace(sent)
        

    return new_sents