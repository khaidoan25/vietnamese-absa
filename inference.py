import os
import json
import torch
import torch.nn.functional as F
from torch.utils.data import DataLoader, TensorDataset
from tqdm import tqdm
import argparse
import numpy as np
from tabulate import tabulate
import tokenization
from utils import normalize_sent
from train import _truncate_seq_pair
from processor import VN_ABSA_pair_Processor, InputExample
from evaluate import convert_examples_to_features_HEAT_no_fix_len
            
class VABSA():
    def __init__(self, args):
        self.model_dir = args.model_dir
        self.vocab_file = os.path.join(args.bert_dir, "vocab.txt")
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.type = args.type
        self.config_dir = args.config_dir

    def load_model(self):
        model_path = os.path.join(self.model_dir, f"{self.type}_model")
        self.model = torch.load(model_path, map_location="cpu")
        self.model.to(self.device)
        self.tokenizer = tokenization.FullTokenizer(
            vocab_file=self.vocab_file, do_lower_case=True
        )

    def get_tokens(self):
        tokens = []
        token_dict = {}
        tokens.append("[CLS]")
        for token in self.tokenizer.tokenize(self.sentence):
            tokens.append(token)
        tokens.append("[SEP]")
        for i, token in enumerate(tokens):
            token_dict[token] = i

        return token_dict

    def predict(self, sentence, max_seq_length=500):
        self.sentence = sentence
        aspects = json.load(open(os.path.join(self.config_dir, f"{self.type}_aspects_vi.json"), "r", encoding='utf-8')).keys()
        processor = VN_ABSA_pair_Processor()
        label_map = processor.get_labels(self.config_dir)

        sentence = normalize_sent([sentence])[0]
        examples = []
        for aspect in aspects:
            examples.append(InputExample(guid=None, text_a=sentence, text_b=aspect, label="none"))

        features = convert_examples_to_features_HEAT_no_fix_len(examples, label_map, self.tokenizer)
        self.model.eval()
        predicted_label = []
        for feature in features:
            input_ids = torch.tensor([feature.input_ids], dtype=torch.long).to("cuda" if torch.cuda.is_available() else "cpu")
            input_mask = torch.tensor([feature.input_mask], dtype=torch.long).to("cuda" if torch.cuda.is_available() else "cpu")
            segment_ids = torch.tensor([feature.segment_ids], dtype=torch.long).to("cuda" if torch.cuda.is_available() else "cpu")
            sent_ids = torch.tensor([feature.sent_ids], dtype=torch.float).to("cuda" if torch.cuda.is_available() else "cpu")
            asp_ids = torch.tensor([feature.asp_ids], dtype=torch.float).to("cuda" if torch.cuda.is_available() else "cpu")
            with torch.no_grad():
                _, logits, _, _ = self.model(input_ids, segment_ids, input_mask, sent_ids, asp_ids)
            output = np.argmax(logits.to('cpu'))
            predicted_label.append(output)

        print(f"Sentence: {sentence}")
        print(f"Prediction:")
        result = []
        for aspect, label in zip(aspects, predicted_label):
            if label == 0:
                new_label = "positive"
            elif label == 1:
                new_label = "negative"
            elif label == 2:
                new_label = "neutral"
            else:
                new_label = "none"
            
            if new_label != "none":
                result.append([aspect, new_label])

        print(tabulate(result, headers=["Aspect", "Sentiment"], tablefmt="psql"))

        return 1

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Vietnamese Aspect-Based Sentiment Analysis.", allow_abbrev=True)
    parser.add_argument("--model_dir", type=str, required=True, help="Model directory.")
    parser.add_argument("--sent", type=str, required=True, help="Sentence to be analysed.")
    parser.add_argument("--type", default="restaurant", type=str, help="Domain of the sentence.")
    parser.add_argument("--config_dir", type=str, required=True, help="Configuration directory.")
    parser.add_argument("--bert_dir", type=str, required=True, help="BERT directory.")
    args = parser.parse_args()
    # model_name = "model_conv_bilstm_attn_new_upsampling_8_reg_rest_1"
    vabsa = VABSA(args)
    vabsa.load_model()

    vabsa.predict(args.sent)