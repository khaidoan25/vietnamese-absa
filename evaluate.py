import os
import argparse
import json
import torch
import numpy as np
import pandas as pd
from tqdm import tqdm
from sklearn.metrics import classification_report

import tokenization
from processor import VN_ABSA_pair_Processor
from train import _truncate_seq_pair, InputFeatures_HEAT


def convert_examples_to_features_HEAT_no_fix_len(examples, label_map, tokenizer, max_seq_length=500):
    """
        Load a data file into a list of InputBatchs.
    """

    # label_map = {}
    # for i, label in enumerate(label_list):
    #     label_map[label] = i

    features = []
    for index, example in enumerate(tqdm(examples)):
        tokens_a = tokenizer.tokenize(example.text_a)

        tokens_b = None
        if example.text_b:
            # Check whether it is bert pair case or bert single case
            tokens_b = tokenizer.tokenize(example.text_b)

        if tokens_b:
            # BERT pair case
            # Modifies `tokens_a` and `tokens_b` in place so that the total
            # length is less than the specified length.
            # Account for [CLS], [SEP], [SEP] with "- 3"
            _truncate_seq_pair(tokens_a, tokens_b, max_seq_length - 3)
        else:
            # BERT single case
            # Account for [CLS] and [SEP] with "- 2"
            if len(tokens_a) > max_seq_length - 2:
                tokens_a = tokens_a[0:(max_seq_length - 2)]

        # The convention in BERT is:
        # (a) For sequence pairs:
        #  tokens:   [CLS] is this jack ##son ##ville ? [SEP] no it is not . [SEP]
        #  type_ids: 0   0  0    0    0     0       0 0    1  1  1  1   1 1
        # (b) For single sequences:
        #  tokens:   [CLS] the dog is hairy . [SEP]
        #  type_ids: 0   0   0   0  0     0 0
        #
        # Where "type_ids" are used to indicate whether this is the first
        # sequence or the second sequence. The embedding vectors for `type=0` and
        # `type=1` were learned during pre-training and are added to the wordpiece
        # embedding vector (and position vector). This is not *strictly* necessary
        # since the [SEP] token unambigiously separates the sequences, but it makes
        # it easier for the model to learn the concept of sequences.
        #
        # For classification tasks, the first vector (corresponding to [CLS]) is
        # used as as the "sentence vector". Note that this only makes sense because
        # the entire model is fine-tuned.
        tokens = []
        segment_ids = []
        sent_ids = []
        asp_ids = []
        tokens.append("[CLS]")
        segment_ids.append(0)
        sent_ids.append(0)
        asp_ids.append(0)
        for token in tokens_a:
            tokens.append(token)
            segment_ids.append(0)
            sent_ids.append(1)
            asp_ids.append(0)
        tokens.append("[SEP]")
        segment_ids.append(0)
        sent_ids.append(0)
        asp_ids.append(0)

        if tokens_b:
            # BERT pair case
            for token in tokens_b:
                tokens.append(token)
                segment_ids.append(1)
                sent_ids.append(0)
                asp_ids.append(1)
            tokens.append("[SEP]")
            segment_ids.append(1)
            sent_ids.append(0)
            asp_ids.append(0)

        input_ids = tokenizer.convert_tokens_to_ids(tokens)

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        input_mask = [1] * len(input_ids)

        label_id = label_map[example.label]
        if label_id != 3:
            asp_label = 1
        else:
            asp_label = 0

        features.append(
                InputFeatures_HEAT(
                        input_ids=input_ids,
                        input_mask=input_mask,
                        segment_ids=segment_ids,
                        sent_ids=sent_ids,
                        asp_ids=asp_ids,
                        asp_label=asp_label,
                        label_id=label_id))

    return features

def load_model(args):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model_path = os.path.join(args.model_dir, f"{args.type}_model")
    model = torch.load(model_path, map_location="cpu")
    model.to(device)
    model.eval()
    
    return model

def get_inference(args, model):
    processor = VN_ABSA_pair_Processor()
    label_map = processor.get_labels(args.config_dir)
    tokenizer = tokenization.FullTokenizer(
        vocab_file=os.path.join(args.bert_dir, "vocab.txt"), do_lower_case=True
    )
    test_examples = processor.get_test_examples(args.data_dir, args.type)

    predicted_label = []
    label_ids = []
    for idx in tqdm(range(len(test_examples[:12]))):
        # test_features = convert_examples_to_features([test_examples[idx]], label_map, max_seq_length, tokenizer)
        test_features = convert_examples_to_features_HEAT_no_fix_len([test_examples[idx]], label_map, tokenizer, max_seq_length=500)
        
        input_ids = torch.tensor([test_features[0].input_ids], dtype=torch.long).to("cuda" if torch.cuda.is_available() else "cpu")
        input_mask = torch.tensor([test_features[0].input_mask], dtype=torch.long).to("cuda" if torch.cuda.is_available() else "cpu")
        segment_ids = torch.tensor([test_features[0].segment_ids], dtype=torch.long).to("cuda" if torch.cuda.is_available() else "cpu")
        sent_ids = torch.tensor([test_features[0].sent_ids], dtype=torch.float).to("cuda" if torch.cuda.is_available() else "cpu")
        asp_ids = torch.tensor([test_features[0].asp_ids], dtype=torch.float).to("cuda" if torch.cuda.is_available() else "cpu")
        label_id = torch.tensor([test_features[0].label_id], dtype=torch.long).to("cuda" if torch.cuda.is_available() else "cpu")

        with torch.no_grad():
            _, logits, _, _ = model(input_ids, segment_ids, input_mask, sent_ids, asp_ids)
        output = np.argmax(logits.to('cpu'))
        predicted_label.append(output)
        label_ids.append(label_id.to('cpu'))

    sentiment_label = []
    sentiment_predict = []
    docs = []
    aspects = []
    for sample, predict, label in zip(test_examples, predicted_label, label_ids):
        if label == 0:
            sentiment_label.append("positive")
        elif label == 1:
            sentiment_label.append("negative")
        elif label == 2:
            sentiment_label.append("neutral")
        else:
            sentiment_label.append("none")

        if predict == 0:
            sentiment_predict.append("positive")
        elif predict == 1:
            sentiment_predict.append("negative")
        elif predict == 2:
            sentiment_predict.append("neutral")
        else:
            sentiment_predict.append("none")

        docs.append(sample.text_a)
        aspects.append(sample.text_b)

    result = pd.DataFrame()
    result["Doc"] = docs
    result["Aspect"] = aspects
    result["Label"] = sentiment_label
    result["Predict"] = sentiment_predict

    return result

def get_evaluation(args, result):
    label_to_idx = json.load(open(os.path.join(args.config_dir, f"{args.type}_label_to_idx.json"), "r", encoding="utf-8"))
    aspect_to_idx = json.load(open(os.path.join(args.config_dir, f"{args.type}_aspects_vi.json"), "r", encoding="utf-8"))
    num_labels = len(label_to_idx)
    num_aspects = len(aspect_to_idx)

    idx = 0
    label = np.zeros(num_labels)
    predict = np.zeros(num_labels)
    y_pred = []
    y_label = []
    for aspect, lb, pred in zip(result["Aspect"], result["Label"], result["Predict"]):
        if idx == num_aspects:
            y_pred.append(predict)
            y_label.append(label)
            idx = 0
            label = np.zeros(num_labels)
            predict = np.zeros(num_labels)
        if lb != "none":
            label[label_to_idx[f"{aspect}, {lb}"]] += 1
        if pred != "none":
            predict[label_to_idx[f"{aspect}, {pred}"]] += 1
        idx += 1
    y_pred.append(predict)
    y_label.append(label)

    print("SENTIMENT POLARITY EVALUATION !!!")
    print(classification_report(y_label, y_pred))

    idx = 0
    label = np.zeros(num_aspects)
    predict = np.zeros(num_aspects)
    y_pred = []
    y_label = []
    for aspect, lb, pred in zip(result["Aspect"], result["Label"], result["Predict"]):
        if idx == num_aspects:
            y_pred.append(predict)
            y_label.append(label)
            idx = 0
            label = np.zeros(num_aspects)
            predict = np.zeros(num_aspects)
        if lb != "none":
            label[aspect_to_idx[f"{aspect}"]] += 1
        if pred != "none":
            predict[aspect_to_idx[f"{aspect}"]] += 1
        idx += 1
    y_pred.append(predict)
    y_label.append(label)

    print("ASPECT CATEGORY DETECTION EVALUATION !!!")
    print(classification_report(y_label, y_pred))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_dir", type=str, required=True, help="Model directory.")
    parser.add_argument("--data_dir", type=str, required=True, help="Dataset directory.")
    parser.add_argument("--type", default="restaurant", required=True, type=str, help="Domain of the dataset.")
    parser.add_argument("--bert_dir", type=str, required=True, help="BERT directory.")
    parser.add_argument("--config_dir", type=str, default="Data/json_config", help="Configuration directory.")
    args = parser.parse_args()

    model = load_model(args)
    result = get_inference(args, model)
    get_evaluation(args, result)